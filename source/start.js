
// The `enyo.ready` method
// -----------------------
// Some things need to happen only after the `document` has been fully
// initialized--when we are sure that the entire source has been loaded.
// You may call `enyo.ready` and pass it a function (and an optional
// context) as many times as you need to and those methods will only be
// executed once the `document` is completely ready.
enyo.ready(function () {
	// This is a very imporant declaration, in which we instantiate our
	// application. We arbitrarily assign the instance to a global
	// variable called `app`, in case we need to refer to it later. All
	// other components of this application are instantiated in the
	// `Sample` namespace. See the `apps/Sample.js` file for more
	// information on namespaces and what this actually means.
	app = new Sample.Application();
	
	// Do all kinds of things here before actually rendering the app

	app.render();
});
