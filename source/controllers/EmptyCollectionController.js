
// The `Sample.PanelsController` kind
// -------------------------------
// This is the kind definition for the global singleton application
// panels controller (`Sample.panels`) instantiated in `Sample.js`.
enyo.kind({
	// We give the kind a name in the application namespace.
	name: "Sample.EmptyCollectionController",
	// We base this kind on `enyo.CollectionController` because we know
	// we need the collection controller's built-in methods. Even
	// though this is an `enyo.CollectionController`, we can add
	// functionality because it's ultimately just an `enyo.Controller`
	// with additional features.
	kind: enyo.CollectionController,
	// We already know what the collection kind is going to be for this
	// controller. Furthermore, we know that there will not be a shared
	// global instance of the collection. We will hand this controller
	// a kind reference and it will instantiate the collection kind when
	// it is created.
	collection: "Sample.EmptyCollection",
	// When the `"Add"` button is tapped, we add a new model to the
	// collection.  Its properties have the default values defined in
	// `enyo.Model`.
	addModel: function () {
		// Here we call the `add` method that is proxying the underlying
		// collection API. Because we pass in an empty hash, it knows to
		// create the model using all of the defaults.
		this.add({});
	},
	// Selects our next index.
	next: function () {
		// Notice we're using and setting our local `index`
		// property, which is part of a two-way binding.
		var idx = this.get("index");
		var len = this.get("length");
		// If the next value for index goes beyond the number of
		// available panels, reset it to 0 so we can start over.
		if (idx+1 === len) {
			this.set("index", 0);
		} else {
			// Otherwise, increment the index and move forward.
			this.set("index", ++idx);
		}
	}
});
